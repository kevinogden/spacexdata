export interface Links {
  article_link?: string;
  presskit: string;
  reddit_media?: string;
  video_link?: string;
  wikipedia?: string;
}
